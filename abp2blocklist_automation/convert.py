# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import sys
import logging

from . import batch
from . import metadata_txt
from . import loading


DEFAULT_ABP2BLOCKLIST_PATH = os.getenv('ABP2BLOCKLIST_PATH')


def parse_args():
    """Setup and parse the command line arguments.

    Returns
    -------
    argparse.Namespace
        With the parsed arguments.

    """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-v', '--verbose', action='store_true',
        help='Whether to log the inner workings of this script to stderr or '
             'not.',
    )

    parser.add_argument(
        '--abp2blocklist', default=DEFAULT_ABP2BLOCKLIST_PATH,
        help='Path to the local `abp2blocklist` folder',
    )
    parser.add_argument('-d', help='Path to the local directory where the '
                                   'filter lists can be found.',
                        default=None)

    parser.add_argument(
        '--skip-metadata', default=False, action='store_true',
        help='Whether or not to include metadata in the conversion.',
    )

    parser.add_argument(
        '--compress', default=False, action='store_true',
        help='Whether or not to compress the output to ZIP.',
    )

    subparsers = parser.add_subparsers()

    # Subparser for converting and merging a number of filter lists.
    txt_subparser = subparsers.add_parser(
        'txt',
        help='Convert and merge a set of text files into JSON and add the '
             'appropriate metadata.',
    )
    txt_subparser.add_argument(
        '--remote',
        help='Fallback domain where the filterlists should be found.',
        default=loading.DEFAULT_FALLBACK_ENDPOINT,
    )
    txt_subparser.set_defaults(runnable=metadata_txt.main)
    txt_subparser.add_argument(
        'input', nargs='+', help='The filterlists that are used as inputs.',
    )
    txt_subparser.add_argument('output', help='Output path.')
    txt_subparser.add_argument(
        'expires',
        help='Information about when the result will become obsolete.',
    )

    # Subparser for handling a set of `txt` commands.
    batch_subparser = subparsers.add_parser(
        'batch',
        help='Handle a batch of `txt`-like requests.',
    )
    batch_subparser.set_defaults(runnable=batch.main)
    batch_subparser.add_argument(
        'operations',
        help='Path to a json file listing the operations that have to be '
             'performed.',
    )
    batch_subparser.add_argument(
        '-o', '--output', default=os.getcwd(),
        help='Path to the parent directory where the resulting blocking lists '
             'will be saved.',
    )

    return parser.parse_args()


def main():
    args = parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.INFO, stream=sys.stderr)

    if 'runnable' in args.__dict__:
        if args.abp2blocklist is None:
            raise Exception(
                'No path to the content blocking list converter found! Please'
                ' specify one either by using the `--abp2blocklist` option or'
                ' the `ABP2BLOCKLIST_PATH` environment variable.',
            )
        args.runnable(args)


if __name__ == '__main__':
    main()
