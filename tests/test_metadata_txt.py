# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.


import subprocess
import json
import os.path
import zipfile

import pytest

import abp2blocklist_automation.metadata_txt as converter
import abp2blocklist_automation.loading as loading
from .conftest import (
    mock_json_loads, mockdownloader, MockPOpenError, MockPOpen,
)


@pytest.fixture
def monkeypatch_setup(monkeypatch):
    old_loads = json.loads
    monkeypatch.setattr(subprocess, 'Popen', MockPOpen)
    monkeypatch.setattr(loading, '_download_filter_list', mockdownloader)
    monkeypatch.setattr(json, 'loads', mock_json_loads)

    return {'old_loads': old_loads}


def test_convert_single_valid(out_dir, monkeypatch_setup, monkeypatch):
    inputs = [loading.get_filter_list_contents('fl.txt')]
    output = out_dir.join('result.json')
    expires = '3'
    skip_metadata = False
    compress = False

    converter.convert(inputs,
                      str(output),
                      expires,
                      '',
                      skip_metadata,
                      compress)
    monkeypatch.setattr(json, 'loads', monkeypatch_setup['old_loads'])

    result = json.load(output)

    assert result['rules'] == {'foo': 'bar', 'baz': 'bam'}
    assert result['expires'] == '3 days'
    assert result['sources'] == [{
        'version': '10',
        'url': 'https://easylist-downloads.adblockplus.org/fl.txt',
    }]


def test_convert_multiple_valid(out_dir, monkeypatch_setup, monkeypatch,
                                filters_dir):
    inputs = [
        loading.get_filter_list_contents('fl_1.txt'),
        loading.get_filter_list_contents('fl_2.txt', None, 'https://foo.com'),
        loading.get_filter_list_contents('fl_3.txt', str(filters_dir))
    ]
    output = out_dir.join('result.json')
    expires = '1'
    skip_metadata = False
    compress = False

    converter.convert(inputs,
                      str(output),
                      expires,
                      '',
                      skip_metadata,
                      compress)

    monkeypatch.setattr(json, 'loads', monkeypatch_setup['old_loads'])

    result = json.load(output)

    assert result['rules'] == {'foo': 'bar', 'baz': 'bam'}
    assert result['expires'] == '1 day'
    assert result['sources'] == [
        {'version': '10',
         'url': 'https://easylist-downloads.adblockplus.org/fl_1.txt'},
        {'version': '10', 'url': 'https://foo.com/fl_2.txt'},
        {'version': '10',
         'url': 'https://easylist-downloads.adblockplus.org/fl_3.txt'},
    ]


def test_convert_invalid(out_dir, monkeypatch_setup, monkeypatch):
    inputs = [loading.get_filter_list_contents('fl.txt')]
    output = out_dir.join('result.json')
    expires = '1'
    skip_metadata = False
    compress = False

    monkeypatch.setattr(subprocess, 'Popen', MockPOpenError)

    with pytest.raises(Exception) as err:
        converter.convert(inputs,
                          str(output),
                          expires,
                          '',
                          skip_metadata,
                          compress)

    assert str(err.value) == ('abp2blocklist_automation failed with exit '
                              'code: 1')


def test_convert_multiple_valid_no_metadata(out_dir,
                                            monkeypatch_setup,
                                            monkeypatch,
                                            filters_dir):

    inputs = [
        loading.get_filter_list_contents('fl_1.txt'),
        loading.get_filter_list_contents('fl_2.txt', None, 'https://foo.com'),
        loading.get_filter_list_contents('fl_3.txt', str(filters_dir))
    ]
    output = out_dir.join('result.json')
    expires = '1'
    skip_metadata = True
    compress = False

    converter.convert(inputs,
                      str(output),
                      expires,
                      '',
                      skip_metadata,
                      compress)

    monkeypatch.setattr(json, 'loads', monkeypatch_setup['old_loads'])

    result = json.load(output)

    assert result == {'foo': 'bar', 'baz': 'bam'}


def test_convert_single_valid_no_metadata(out_dir,
                                          monkeypatch_setup,
                                          monkeypatch):

    inputs = [loading.get_filter_list_contents('fl.txt')]
    output = out_dir.join('result.json')
    expires = '3'
    skip_metadata = True
    compress = False

    converter.convert(inputs,
                      str(output),
                      expires,
                      '',
                      skip_metadata,
                      compress)

    monkeypatch.setattr(json, 'loads', monkeypatch_setup['old_loads'])

    result = json.load(output)

    assert result == {'foo': 'bar', 'baz': 'bam'}


def test_convert_valid_compressed(out_dir, monkeypatch_setup, monkeypatch):

    inputs = [loading.get_filter_list_contents('fl.txt')]
    output = out_dir.join('result.json')
    expires = '3'
    skip_metadata = False
    compress = True

    converter.convert(inputs,
                      str(output),
                      expires,
                      '',
                      skip_metadata,
                      compress)

    monkeypatch.setattr(json, 'loads', monkeypatch_setup['old_loads'])

    assert os.path.exists(str(out_dir.join('result.zip')))

    with zipfile.ZipFile(out_dir.join('result.zip'), 'r') as zip_ref:
        zip_ref.extractall(out_dir.join('unzipped'))

    assert os.path.exists(out_dir.join('unzipped'))

    result = json.load(out_dir.join('unzipped').join('result.json'))

    assert result['rules'] == {'foo': 'bar', 'baz': 'bam'}
    assert result['expires'] == '3 days'
    assert result['sources'] == [{
        'version': '10',
        'url': 'https://easylist-downloads.adblockplus.org/fl.txt',
    }]
